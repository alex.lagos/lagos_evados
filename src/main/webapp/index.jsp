<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>lagos_evados</title>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />


        <script> type = "text/javascript" >
                    function textoText() {
                        var info = document.getElementById('textoingresado').value;
                        alert(info);

                    }
        </script>


    </head>
    <body background="resources/forest3.jpg" style="background-size: cover">


        <div class="jumbotron">
            <h4 class="display-4" style="text-align: center">SONIDOS DEL BOSQUE</h4>
            <p class="lead" style="text-align: center">Alex Lagos</p>
            <hr class="my-4">

            <br>

            <div class="row">
                <div class="col-md-2">

                </div>
                <div class="col-md-2">
                    <h3>Gorila</h3>

                    <div class="card">
                        <img class="card-img-top" src="resources/gorila.jpg">
                        <div class="card-body">
                        </div>
                    </div>
                    <audio id="grunido" src="resources/grunido.mp3"></audio>
                    <a class="btn btn-primary btn-lg" onclick="alert('Estás escuchando un gorila...', document.getElementById('grunido').play())" href="#" role="button">Escuchar sonido</a>

                </div>

                <div class="col-md-4">
                </div>

                <div class="col-md-2">
                    <h3>Buho</h3>

                    <div class="card">
                        <img class="card-img-top" src="resources/buho.jpg">
                        <div class="card-body">

                        </div>
                    </div>
                    <audio id="ululato" src="resources/ululato.mp3"></audio>
                    <a class="btn btn-primary btn-lg" onclick="alert('Estás escuchando un buho...', document.getElementById('ululato').play())" href="#" role="button">Escuchar sonido</a>

                </div>
                <div class="col-md-2"> 
                </div>
                <div class="row">


                    <div class="col-md-2">
                        <h3>Oso</h3>

                        <div class="card">
                            <img class="card-img-top" src="resources/oso.jpg">
                            <div class="card-body">

                            </div>
                        </div>
                        <audio id="grunido2" src="resources/grunido2.mp3"></audio>
                        <a class="btn btn-primary btn-lg" onclick="alert('Estás escuchando el gruñido de un oso...', document.getElementById('grunido2').play())" href="#" role="button">Escuchar sonido</a>

                    </div>
                    <div class="col-md-3">
                    </div>

                    <div class="col-md-2">
                        <h3>Jaguar</h3>

                        <div class="card">
                            <img class="card-img-top" src="resources/jaguar.jpg">
                            <div class="card-body">

                            </div>
                        </div>
                        <audio id="grunido3" src="resources/grunido3.mp3"></audio>
                        <a class="btn btn-primary btn-lg" onclick="alert('Estás escuchando el gruñido de un jaguar...', document.getElementById('grunido3').play())" href="#" role="button">Escuchar sonido</a>

                    </div>
                    <div class="col-md-3">     
                    </div>


                    <div class="col-md-2">     
                        <h3>Tigre</h3>

                        <div class="card">
                            <img class="card-img-top" src="resources/tigre.jpg">
                            <div class="card-body">

                            </div>
                        </div>
                        <audio id="Roar" src="resources/Roar.mp3"></audio>
                        <a class="btn btn-primary btn-lg" onclick="alert('Estás escuchando el gruñido de un tigre...', document.getElementById('Roar').play())" href="#" role="button">Escuchar sonido</a>

                    </div>



                    <p class="lead">

                    </p>
                </div>

            </div>
    </body>
</html>
